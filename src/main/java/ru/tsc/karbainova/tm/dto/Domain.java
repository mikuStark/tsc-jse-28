package ru.tsc.karbainova.tm.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.User;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@XmlRootElement
@JsonRootName("domain")
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {
    @NonNull
    @JsonProperty("user")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    private List<User> users;
    @NonNull
    @JsonProperty("project")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projects;
    @NonNull
    @JsonProperty("task")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> tasks;

//    public void setUsers(@NonNull List<User> users) {
//        this.users = users;
//    }
//
//    public void setProjects(@NonNull List<Project> projects) {
//        this.projects = projects;
//    }
//
//    public void setTasks(@NonNull List<Task> tasks) {
//        this.tasks = tasks;
//    }

}
