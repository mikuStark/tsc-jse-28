package ru.tsc.karbainova.tm.api.service;

public interface ServiceLocator {
    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectToTaskService getProjectToTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

    IPropertyService getPropertyService();
}
