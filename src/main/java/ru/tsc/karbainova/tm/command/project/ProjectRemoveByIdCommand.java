package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "remove-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by id project";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectToTaskService().removeById(userId, id);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }
}
