package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.ProjectAbstractCommand;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class ProjectFinishByIndexCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "status-finish-by-index-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish by index";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().finishByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }
}
